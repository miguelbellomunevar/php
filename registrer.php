<?php
require_once("class/user.php");


if(isset($_POST["name"]) && isset($_POST["phone"]) && isset($_POST["email"]) && isset($_POST["password"])){
    $email = $_POST["email"];
    $password= $_POST["password"];
    $phone = $_POST["phone"];
    $name = $_POST["name"];

    $user = new User($name,$email,$phone,$password);

    $user->createUser($name,$email,$phone,$password);
    
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Document</title>
</head>
<body>
    <form action="registrer.php" method="post">
    
        <label for="">Name</label>
        <input action="text" name="name" id="name">
        <hr>
        <label for="">Phone</label>
        <input type="text" name="phone" id="phone" pattern="^[1-9]\d{9}$">
        <hr>
        <label for="">Email</label>
        <input type="email" name="email" id="email" pattern="^[^@]+@[^@]+\.[a-zA-Z]{2,}$">
        <hr>
        <label for="">password</label>
        <input type="text" name="password" id="password">
        <br>
        <input type="submit" value="Registrer">
    </form>

    <a href="index.php">back to login</a>
</body>
</html>