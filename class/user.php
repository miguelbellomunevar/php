<?php
include_once("connection/connection.php");
class User {

    private $name;
    private $email;
    private $phone;
    private $password;

    function __construct($name = "", $email ="", $phone = "", $password= ""){

        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->password = $password;
    }

    /*create a unique user in the file .json*/
    function CreateUser(){
        $connection = new Connection();
        $data = $connection->selectContentFile();
        array_push($data["users"],array("name"=> $this->name,"email" => $this->email,"phone" => $this->phone, "password" => $this->password));
        $connection->addData(json_encode($data));
        return true;
    }

    function SearchUser($email, $password){
        $connection = new Connection();
        $data = $connection->selectContentFile();
        $user = array_search($email, array_column($data["users"], 'email'));
        return $data["users"][$user]["password"] == $password ? true : false;
    }
    
}
?>