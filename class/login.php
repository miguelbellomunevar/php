<?php 
class Login{
    private $login = false;
    private $email;

    function __construct(){
        session_start();
        $this->verifyLogin();
    }
    
    private function verifyLogin(){
        if(isset($_SESSION["email"])){
            $this->email = $_SESSION["email"];
            $this->login = true;
        }else{
            unset($this->email);
            $this->login = false;
        }
    }

    public function starLogin($email){
        if($email){
            $this->email = $_SESSION["email"] = $email;
            $this->login = true;
        }
    }

    public function logout($email){
        unset($_SESSION["email"]);
        unset($this->email);
        $this->login = false;
    }

    public function sessionState(){
        return $this->login;
    }

    public function getUser(){
        return $this->email;
    }
    
}
?>