<?php
include_once("connection/connection.php");
class Post {

    private $filterText;
    private $dateToFilter;

    function __construct($filter = "", $dateToFilter = ""){
        $this->filter = $filter;
        $this->dateToFilter = $dateToFilter;
    }

    private function orderDate($post, $post2){
        return strtotime(trim($post['date'])) < strtotime(trim($post2['date']));
    }

    private function similarText($post){
        if (strpos($post["content"], $this->filter) !== false) {
            return true;
        }
        
    }

    private function filterDate($post){
        return  strtotime(trim($post['date'])) >= strtotime(trim($this->dateToFilter))?  true : false;
    }

    public function createPost($content, $user){
        $connection = new Connection();
        $data = $connection->selectContentFile();
        array_push($data["posts"],array("content"=> $content,"date" => date("Y-m-d H:i:s"),"user" => $user));
        $connection->addData(json_encode($data));
        return true;
    }

    public function filterPostsForContext(){
        $connection = new Connection();
        $data = $connection->selectContentFile();
        $posts = $data["posts"];
        if($this->filter){
            $posts = (array_filter($posts, array('Post','similarText')));
        }
        if($this->dateToFilter){
            $posts = (array_filter($posts, array('Post','filterDate')));
        }
        return $posts;
    }

    public function listPost(){
        $connection = new Connection();
        $data = $connection->selectContentFile();
        $posts = $data["posts"];
        usort($posts,  array('Post','orderDate'));
        return $posts;
    }
}
?>