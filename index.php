<?php

require_once("class/login.php");
$session = new Login();

if($session->sessionState()){
    header("location:home.php");
}else{
    header("location:login.php");
}

?>