<?php

require_once("class/login.php");
require_once("class/post.php");
$session = new Login();
$postClass = new Post();

#check if the session has been started
if($session->sessionState() == false){
    header("location:login.php");
}

if(isset($_POST["post"]) && $_POST["post"] != ""){
    $postClass->createPost($_POST["post"],$session->getUser());
}

if((isset($_POST["search"]) && $_POST["search"] != "") || (isset($_POST["filterDate"]) && $_POST["filterDate"] != "")){
    $postFilter = new Post($_POST["search"], $_POST["filterDate"]);
    $posts =  $postFilter->filterPostsForContext();
}else{
    $posts =  $postClass->listPost();
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Document</title>
</head>
<body>
    <form action="home.php" method="post" class="search">
    <label>search</label>
    <input type="text" name="search" id="search">
    <input type="text" name="filterDate" id="filterDate">
    <input type="submit" value="search">
    </form>
    <div class="posts">
    <?php 
        foreach ($posts as $post ) {
    ?>
        <div class="post-card">
            <p class="date"><?php  echo $post["date"]; ?></p>
            <p class="body"><?php  echo $post["content"];?></p>
            <p class="user"><?php echo $post["user"]?></p>
        </div>

    <?php
        }
    ?>
    </div>
    <form action="home.php" method="post">
        <textarea name="post" id="post" cols="50" rows="10"></textarea>
        <input type="submit" value="create new post" class="create-post">
    </form>
</body>
</html>